package Controlador;

import Negocio.Banco;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrarOperacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            Banco banco = new Banco();            

            int nroCuenta = Integer.parseInt(request.getParameter("nrocuenta"));
            int valor = Integer.parseInt(request.getParameter("valor"));
            int tipoMov = Integer.parseInt(request.getParameter("tipo"));

            boolean flag = false;          

            switch (tipoMov) {
                case 1:                    
                case 2:
                    flag = banco.insertarOperacion(nroCuenta, valor, tipoMov);
                    break;
                case 3:
                    int nroCuentaDestino = Integer.parseInt(request.getParameter("nrocuentadestino"));
                    
                    if (nroCuenta == nroCuentaDestino) {
                        request.getSession().setAttribute("error", "Cuenta Origen y Destino Iguales");
                        request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
                    } else {
                        flag = banco.insertarOperacionT(nroCuenta, nroCuentaDestino, valor);
                    }
                    break;
                default : break;
            }
            
            if (flag) {
                request.getRequestDispatcher("./jsp/Operacion/registroexitoso.jsp").forward(request, response);
            } else {
                request.getSession().setAttribute("error", "Saldo insuficiente");
                request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
            }

        } catch (Exception e) {
            request.getSession().setAttribute("error", e.getMessage());
            request.getRequestDispatcher("./jsp/error/errorSystem.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
