
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Redireccionamiento</title>
    </head>
    <body onload="init()">
        <h1>Iniciando sesión..!</h1>
        <a href="./index.html" style="display: none;" id="lnk">Link</a>
        
        <script>
            function init(){
                document.getElementById("lnk").click();
            }
        </script>
    </body>
</html>
