var request = new XMLHttpRequest();

function pulsar(e) {
  var tecla = (document.all) ? e.keyCode :e.which;  
  if(tecla==13) loadCuenta();
}

function loadCuenta() {    
    request.open("POST", "../../ModificarCuenta.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var view = document.getElementById('view').value;            
            var obj = request.responseText;
            
            if(view === "edit"){
               if(obj === "noexist"){
                   alert("ERROR, CUENTA NO REGISTRADA!");
                   var div = "<div id='dataspace'></div>";
                   document.getElementById("dataspace").innerHTML = div;
               }
               else{                   
                   createeditform(obj);
               }
            }
            else if(view === "delete"){
                if(obj === "noexist"){
                   alert("ERROR, CUENTA NO REGISTRADA!");
                   var div = "<div id='dataspace'></div>";
                   document.getElementById("dataspace").innerHTML = div;
               }
               else{
                   createdeleteform(obj); 
               }
            }

        } else if (request.readyState == 4 && request.status != 200) {
            alert(obj);
        }
    };
    var cuentafind = document.getElementById('cuentafind').value;
    if(cuentafind===""){
        alert("Digite número de cuenta");
        return;
    }
    request.send("action=busqueda&cuentafind=" + cuentafind);
}

function createeditform(obj){
    var json = JSON.parse(obj);
    
    var form = "<h3 class='text-center'> Cambiar datos </h3>";
    form += "<form name='registrar' action='../../ModificarCuenta.do' class='register'>";    
    form += "<input type='hidden' name='nrocuenta' value='"+ json.nrocuenta +"' >";
    
    var tipo = json.tipo;    
    form += "<p>Tipo: Cuenta "+json.tipo+"</p>";    
    form += "<select class='custom-select' id='tipocuenta' name='tipocuenta'>";
    form += "<option> Elegir tipo </option>";
    if(tipo === "ahorros"){
        form += "<option value='1' selected> Cuenta Ahorros</option>";
        form += "<option value='2'> Cuenta Corriente </option>";
    }
    else{        
        form += "<option value='1'> Cuenta Ahorros</option>";
        form += "<option value='2' selected> Cuenta Corriente </option>";
    }
    form += "</select>";
    form += "<hr>";
    
    if(tipo === "corriente"){
        form += "<p>Sobregiro: "+json.sobregiro+"<input class='register-input' type='number' name='sobregiro' value='"+ json.sobregiro +"' required></p>";
    }
    else{
        form += "<input type='hidden' name='sobregiro' value='"+ json.sobregiro +"'>";
        form += "<p>Sobregiro: "+json.sobregiro+"<input class='register-input' type='number' name='sobre' value='"+ json.sobregiro +"' disabled></p>";
    }
    
    var clt = json.cliente;
    form += "<p>Cliente: " + clt + "</p>";    
    form += "<select class='custom-select' id='cedula' name='cedula'>";
    form += "<option> Seleccionar Cliente </option>";
    form += "</select>"    
    loadListClientes(clt);
    form += "<hr>";
    
    form += "<p>Saldo: "+json.saldo+"<input class='register-input'  type='number' name='saldo' value='"+ json.saldo +"' required></p>";    
    form += "<input type='hidden' value='modificar' name='action'>";
    form += "<input type='submit' value='Modificar' name='modificar' class='register-button' >";
    form += "</form>";
    
    document.getElementById("dataspace").innerHTML = form;    
    document.getElementById("lnk").click();
    
}

function createdeleteform(obj){
    var json = JSON.parse(obj);
    var n = json.nombre;
    
    var form = "<h3 class='text-center'> Datos Cuenta </h3>";
    form += "<form name='registrar' action='../../ModificarCuenta.do' class='register'>";    
    form += "<input type='hidden' name='nrocuenta' value='"+ json.nrocuenta +"' >";
    form += "<p>Cliente:<input class='register-input' type='text' value='"+ json.cliente +"' disabled></p>";
    form += "<p>Tipo Cuenta:<input class='register-input'  type='text' value='Cuenta "+ json.tipo +"' disabled></p>";    
    form += "<p>Saldo:<input class='register-input'  type='text' value='"+ json.saldo +"' disabled></p>";
    
    form += "<input type='hidden' value='eliminar' name='action'>";
    form += "<input type='submit' value='Eliminar' name='eliminar' class='register-button' >";
    form += "</form>";
    
    document.getElementById("dataspace").innerHTML = form;
    document.getElementById("lnk").click();    
}

function loadListClientes(clt) {
    var j = null;
    request.open("POST", "../../CrearJson.do", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var obj = request.responseText;
            if (obj === "NO HAY BANCO CREADO") {
                alert(obj);
                return;
            }

            j = JSON.parse(obj);
            var select = "<select name='cedula'>";
            select += "<option>Seleccionar Cliente</option>";
            for (var i = 0; i < j.cliente.length; i++) {
                var cedula = j.cliente[i].cedula;
                var nombre = j.cliente[i].nombre;
                var dato = cedula +" | "+nombre;
                select += "<option id='"+ dato +"' value='"+ cedula + "'>" + dato + "</option>";
            }
            document.getElementById('cedula').innerHTML = select;
            document.getElementById(clt).selected = true;
            
        } else if (request.readyState == 4 && request.status != 200) {
            var message = request.responseText;
            message = request.responseText;
            alert(message);
        }
    };
    request.send("type=clientes");
}